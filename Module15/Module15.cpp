// Module15.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

const int N = 10; // Max number
const bool UNEVEN = true; // Defines what to show in console (true = uneven, false = even)

void EvenOrUneven(int n, bool uneven)   //even definiton function
{
	for (int i = 0; i < n; i++)   // works till to i = N
	{
		 // if  i % 2 = 0  - even, = 1 - uneven 
		if (i % 2 == uneven)
		{
			std::cout << i;      //print even numbers
		}
		
	}
}

int main()
{
	EvenOrUneven(N, UNEVEN); //call function
	
}


